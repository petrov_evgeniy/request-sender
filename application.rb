require 'eventmachine'
require 'em-http-server'
require_relative 'server'

HOST = 'localhost'
PORT = '8089'

EM::run do
  EM::start_server(HOST, PORT, Server)
end