require 'eventmachine'
require 'em-http-server'
require_relative 'server'
require 'net/http'
require 'uri'
require 'launchy'

REQUESTS_COUNT = 200
ACTIVE_THREADS = 10
HOST = 'localhost'
PORT = '8088'

threads = []

$thread = Thread.new do
  EM::run do
    $em_server = EM::start_server(HOST, PORT, Server)
    p 'server started'
  end
end

(1..REQUESTS_COUNT).each_slice(ACTIVE_THREADS) { |n|
    n.each {
      threads << Thread.new do
        url = Random.new.rand > 0.3 ? 'http://ya.ru' : 'http://not_found.ru' #70% should succeded, 30% should fail
        Net::HTTP.get_response(URI.parse("http://#{HOST}:#{PORT}/send?url=#{url}"))
      end
    }
    threads.each { |thread| thread.join }
    threads.clear
}
threads.each { |thread| thread.join }

Launchy.open("http://#{HOST}:#{PORT}/charts")
sleep 10  #wait for browser and stop server
EM::stop_server($em_server)
Thread.kill($thread)