require 'rubygems'
require 'eventmachine'
require 'em-http-server'
require 'em-http'
require_relative 'time_helper'

$all_requests = []
$success_requests = []
$fail_requests = []

class Server < EM::HttpServer::Server
  AFTER_FAIL_TIME_INTERVAL = 5 # wait 5 seconds and make new request

  def request(url)
    EM::HttpRequest.new(url).get
    .callback {
      $success_requests << [Time.now, $success_requests.count + 1]
    }
    .errback {
      $fail_requests << [Time.now, $fail_requests.count + 1]
      EM.add_timer(AFTER_FAIL_TIME_INTERVAL) { request url }
    }
  end

  def process_http_request
    response = EM::DelegatedHttpResponse.new(self)
    case @http_request_uri
      when '/send'
        url = @http_query_string
                .split('&')
                .map { |param| param.split('=') }
                .select { |param| param.first == 'url' }
                .last.last rescue nil
        if url
          response.status = 200
          response.content_type 'text/html'
          response.content = 'OK'
          $all_requests << [Time.now, $all_requests.count + 1]
          request url
        else
          response.status = 500
        end
      when '/charts'
        response.status = 200
        response.content_type 'text/html'
        data = File.read('views/chart.html')
                   .gsub('@all_requests', process_time($all_requests).to_s)
                   .gsub('@success_requests', process_time($success_requests).to_s)
                   .gsub('@fail_requests', process_time($fail_requests).to_s)
        response.content = data
        response.send_response
      else
        response.status = 404
    end
    response.send_response

  end

  def process_time(array)
    result = []
    return result if array.empty?
    (0..array.length - 1).each { |i|
      one = i == 0 ? array[i] : array[i-1]
      interval = one.first.step(array[i].first)
      interval_value = one.last
      result += interval.map { |time| [time, interval_value] }
    }
    result << array.last
    start_time = array.first.first
    result.map! { |r| [r.first - start_time, r.last]}
  end

end