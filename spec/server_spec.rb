require_relative 'spec_helper'
require_relative '../server'

HOST = 'localhost'
PORT = '8089'

describe 'Server#process_http_request' do

  before :all do
    $thread = Thread.new do
      EM::run do
        $em_server = EM::start_server(HOST, PORT, Server)
        p 'server(testing sending) started'
      end
    end
  end

  after :all do
    sleep(2) #wait for callbacks end
    EM::stop_server($em_server)
    Thread.kill($thread)
  end

  it 'should respond 200 status code if there is no such web site' do
    response = Net::HTTP.get_response(URI.parse("http://#{HOST}:#{PORT}/send?url=http://blablabla"))
    response.code.to_i.should == 200
  end

  it 'should respond 200 status code and OK body for known web site' do
    response = Net::HTTP.get_response(URI.parse("http://#{HOST}:#{PORT}/send?url=http://ya.ru"))
    response.code.to_i.should == 200
    response.body.should == 'OK'
  end

  it 'should respond 200 status code and OK body if there is additional params' do
    response = Net::HTTP.get_response(URI.parse("http://#{HOST}:#{PORT}/send?url=http://ya.ru&x=1"))
    response.code.to_i.should == 200
    response.body.should == 'OK'
  end

  it 'should respond 500 status code if there is no url parameter' do
    response = Net::HTTP.get_response(URI.parse("http://#{HOST}:#{PORT}/send?not_url_param=http://ya.ru"))
    response.code.to_i.should == 500
  end

  it 'should respond 500 status code if params are empty' do
    response = Net::HTTP.get_response(URI.parse("http://#{HOST}:#{PORT}/send"))
    response.code.to_i.should == 500
  end

  it 'should respond 200 status code for charts' do
    response = Net::HTTP.get_response(URI.parse("http://#{HOST}:#{PORT}/charts"))
    response.code.to_i.should == 200
  end

  it 'should respond 404 status code otherwise' do
    response = Net::HTTP.get_response(URI.parse("http://#{HOST}:#{PORT}/not_found"))
    response.code.to_i.should == 404
  end

end
