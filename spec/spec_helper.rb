require 'rspec'
require 'eventmachine'
require 'em-http-server'
require 'net/http'
require 'uri'
require 'simplecov'

SimpleCov.start do
  add_filter '/spec/'
  add_filter '/coverage/'
end