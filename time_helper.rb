class Time
  def step(to, interval_in_seconds = 1)
    from = self
    times = []
    while from < to do
      times << from
      from += interval_in_seconds
    end
    times
  end
end